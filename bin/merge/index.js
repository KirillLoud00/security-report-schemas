import fs from 'fs';
import merger from 'json-schema-merge-allof';
import refparser from 'json-schema-ref-parser';
import util from 'util';

if (process.argv.length != 3) {
  console.log("Schema file is required");
  process.exit(1);
}

const schemaFile = process.argv[2];

try {
  const stat = fs.statSync(schemaFile);
} catch (e) {
  console.log("Error reading schema file:", e.code);
  process.exit(e.errno);
}

(async () => {
  const f = schemaFile;
  const p =  new refparser();
  const o = await p.dereference(f);
  const r = merger(o);
  console.log(JSON.stringify(r));
})(); 
