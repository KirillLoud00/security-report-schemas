#!/bin/bash

PROJECT_DIRECTORY=$(realpath "$(dirname "$(realpath "${BASH_SOURCE[0]}")")/..")
SECRETS_SCHEMA="$PROJECT_DIRECTORY/dist/secret-detection-report-format.json"

source "$PROJECT_DIRECTORY/test/helper_functions.sh"

setup_suite() {
  regenerate_dist_schemas
}

test_secrets_contains_common_definitions() {
  verify_schema_contains_selector "$SECRETS_SCHEMA" ".title"
  verify_schema_contains_selector "$SECRETS_SCHEMA" ".description"
  verify_schema_contains_selector "$SECRETS_SCHEMA" ".self.version"
  verify_schema_contains_selector "$SECRETS_SCHEMA" ".properties.scan.properties.end_time"
  verify_schema_contains_selector "$SECRETS_SCHEMA" ".properties.scan.properties.messages"
  verify_schema_contains_selector "$SECRETS_SCHEMA" ".properties.scan.properties.scanner"
  verify_schema_contains_selector "$SECRETS_SCHEMA" ".properties.scan.properties.start_time"
  verify_schema_contains_selector "$SECRETS_SCHEMA" ".properties.scan.properties.status"
  verify_schema_contains_selector "$SECRETS_SCHEMA" ".properties.scan.properties.type"
  verify_schema_contains_selector "$SECRETS_SCHEMA" '.properties.scan.properties.type.enum == ["secret_detection"]'
  verify_schema_contains_selector "$SECRETS_SCHEMA" ".properties.schema"
  verify_schema_contains_selector "$SECRETS_SCHEMA" ".properties.version"
  verify_schema_contains_selector "$SECRETS_SCHEMA" ".properties.vulnerabilities.items.properties.category"
  verify_schema_contains_selector "$SECRETS_SCHEMA" ".properties.vulnerabilities.items.properties.name"
  verify_schema_contains_selector "$SECRETS_SCHEMA" ".properties.vulnerabilities.items.properties.message"
  verify_schema_contains_selector "$SECRETS_SCHEMA" ".properties.vulnerabilities.items.properties.description"
  verify_schema_contains_selector "$SECRETS_SCHEMA" ".properties.vulnerabilities.items.properties.cve"
  verify_schema_contains_selector "$SECRETS_SCHEMA" ".properties.vulnerabilities.items.properties.severity"
  verify_schema_contains_selector "$SECRETS_SCHEMA" ".properties.vulnerabilities.items.properties.confidence"
  verify_schema_contains_selector "$SECRETS_SCHEMA" ".properties.vulnerabilities.items.properties.solution"
  verify_schema_contains_selector "$SECRETS_SCHEMA" ".properties.vulnerabilities.items.properties.scanner"
  verify_schema_contains_selector "$SECRETS_SCHEMA" ".properties.vulnerabilities.items.properties.identifiers"
  verify_schema_contains_selector "$SECRETS_SCHEMA" ".properties.vulnerabilities.items.properties.links"
  verify_schema_contains_selector "$SECRETS_SCHEMA" ".properties.remediations.items.properties.fixes"
  verify_schema_contains_selector "$SECRETS_SCHEMA" ".properties.remediations.items.properties.summary"
  verify_schema_contains_selector "$SECRETS_SCHEMA" ".properties.remediations.items.properties.diff"
}

test_should_not_contain_definitions() {
  verify_schema_doesnt_contain_selector "$SECRETS_SCHEMA" ".definitions"
}

test_secrets_extensions() {
  verify_schema_contains_selector "$SECRETS_SCHEMA" 'select(.properties.vulnerabilities.items.required[] | contains("location"))'

  verify_schema_contains_selector "$SECRETS_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.file"
  verify_schema_contains_selector "$SECRETS_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.start_line"
  verify_schema_contains_selector "$SECRETS_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.end_line"
  verify_schema_contains_selector "$SECRETS_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.method"
  verify_schema_contains_selector "$SECRETS_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.class"

  verify_schema_contains_selector "$SECRETS_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.commit.properties.author"
  verify_schema_contains_selector "$SECRETS_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.commit.properties.date"
  verify_schema_contains_selector "$SECRETS_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.commit.properties.message"
  verify_schema_contains_selector "$SECRETS_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.commit.properties.sha"
  verify_schema_contains_selector "$SECRETS_SCHEMA" ".properties.vulnerabilities.items.properties.raw_source_code_extract"
}
