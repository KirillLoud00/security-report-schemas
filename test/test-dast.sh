#!/bin/bash

PROJECT_DIRECTORY=$(realpath "$(dirname "$(realpath "${BASH_SOURCE[0]}")")/..")
DAST_SCHEMA="$PROJECT_DIRECTORY/dist/dast-report-format.json"

source "$PROJECT_DIRECTORY/test/helper_functions.sh"

setup_suite() {
  regenerate_dist_schemas
}

test_dast_contains_common_definitions() {
  verify_schema_contains_selector "$DAST_SCHEMA" ".title"
  verify_schema_contains_selector "$DAST_SCHEMA" ".description"
  verify_schema_contains_selector "$DAST_SCHEMA" ".self.version"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.scan.properties.end_time"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.scan.properties.messages"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.scan.properties.scanner"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.scan.properties.start_time"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.scan.properties.status"
  verify_schema_contains_selector "$DAST_SCHEMA" '.properties.scan.properties.type.enum == ["dast", "api_fuzzing"]'
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.schema"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.version"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.category"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.name"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.message"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.description"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.cve"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.severity"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.confidence"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.solution"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.scanner"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.identifiers"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.links"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.discovered_at"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.remediations.items.properties.fixes"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.remediations.items.properties.summary"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.remediations.items.properties.diff"
}

test_should_not_contain_definitions() {
  verify_schema_doesnt_contain_selector "$DAST_SCHEMA" ".definitions"
}

test_dast_extensions() {
  verify_schema_contains_selector "$DAST_SCHEMA" 'select(.properties.vulnerabilities.items.required[] | contains("location"))'

  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.hostname"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.param"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.method"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.path"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.evidence.properties.source.properties.id"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.evidence.properties.source.properties.name"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.evidence.properties.source.properties.url"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.evidence.properties.summary"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.evidence.properties.request.properties.headers"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.evidence.properties.request.properties.method"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.evidence.properties.request.properties.url"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.evidence.properties.request.properties.body"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.evidence.properties.response.properties.headers"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.evidence.properties.response.properties.reason_phrase"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.evidence.properties.response.properties.status_code"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.evidence.properties.response.properties.body"

  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.scan.properties.scanned_resources.items.properties.method"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.scan.properties.scanned_resources.items.properties.url"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.scan.properties.scanned_resources.items.properties.type"

  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.evidence.properties.supporting_messages.items.properties.name"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.evidence.properties.supporting_messages.items.properties.request.properties.headers"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.evidence.properties.supporting_messages.items.properties.request.properties.method"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.evidence.properties.supporting_messages.items.properties.request.properties.url"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.evidence.properties.supporting_messages.items.properties.request.properties.body"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.evidence.properties.supporting_messages.items.properties.response.properties.headers"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.evidence.properties.supporting_messages.items.properties.response.properties.reason_phrase"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.evidence.properties.supporting_messages.items.properties.response.properties.status_code"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.evidence.properties.supporting_messages.items.properties.response.properties.body"

  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.assets.items.properties.type"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.assets.items.properties.name"
  verify_schema_contains_selector "$DAST_SCHEMA" ".properties.vulnerabilities.items.properties.assets.items.properties.url"

}
