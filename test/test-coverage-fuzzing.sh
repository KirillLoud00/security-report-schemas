#!/bin/bash

PROJECT_DIRECTORY=$(realpath "$(dirname "$(realpath "${BASH_SOURCE[0]}")")/..")
COVERAGE_FUZZING_SCHEMA="$PROJECT_DIRECTORY/dist/coverage-fuzzing-report-format.json"

source "$PROJECT_DIRECTORY/test/helper_functions.sh"

setup_suite() {
  regenerate_dist_schemas
}

test_coverage_fuzz_contains_common_definitions() {
  verify_schema_contains_selector "$COVERAGE_FUZZING_SCHEMA" ".title"
  verify_schema_contains_selector "$COVERAGE_FUZZING_SCHEMA" ".description"
  verify_schema_contains_selector "$COVERAGE_FUZZING_SCHEMA" ".self.version"
  verify_schema_contains_selector "$COVERAGE_FUZZING_SCHEMA" ".properties.scan.properties.end_time"
  verify_schema_contains_selector "$COVERAGE_FUZZING_SCHEMA" ".properties.scan.properties.messages"
  verify_schema_contains_selector "$COVERAGE_FUZZING_SCHEMA" ".properties.scan.properties.scanner"
  verify_schema_contains_selector "$COVERAGE_FUZZING_SCHEMA" ".properties.scan.properties.start_time"
  verify_schema_contains_selector "$COVERAGE_FUZZING_SCHEMA" ".properties.scan.properties.status"
  verify_schema_contains_selector "$COVERAGE_FUZZING_SCHEMA" '.properties.scan.properties.type.enum == ["coverage_fuzzing"]'
  verify_schema_contains_selector "$COVERAGE_FUZZING_SCHEMA" ".properties.schema"
  verify_schema_contains_selector "$COVERAGE_FUZZING_SCHEMA" ".properties.version"
  verify_schema_contains_selector "$COVERAGE_FUZZING_SCHEMA" ".properties.vulnerabilities.items.properties.category"
  verify_schema_contains_selector "$COVERAGE_FUZZING_SCHEMA" ".properties.vulnerabilities.items.properties.name"
  verify_schema_contains_selector "$COVERAGE_FUZZING_SCHEMA" ".properties.vulnerabilities.items.properties.message"
  verify_schema_contains_selector "$COVERAGE_FUZZING_SCHEMA" ".properties.vulnerabilities.items.properties.description"
  verify_schema_contains_selector "$COVERAGE_FUZZING_SCHEMA" ".properties.vulnerabilities.items.properties.cve"
  verify_schema_contains_selector "$COVERAGE_FUZZING_SCHEMA" ".properties.vulnerabilities.items.properties.severity"
  verify_schema_contains_selector "$COVERAGE_FUZZING_SCHEMA" ".properties.vulnerabilities.items.properties.confidence"
  verify_schema_contains_selector "$COVERAGE_FUZZING_SCHEMA" ".properties.vulnerabilities.items.properties.solution"
  verify_schema_contains_selector "$COVERAGE_FUZZING_SCHEMA" ".properties.vulnerabilities.items.properties.scanner"
  verify_schema_contains_selector "$COVERAGE_FUZZING_SCHEMA" ".properties.vulnerabilities.items.properties.identifiers"
  verify_schema_contains_selector "$COVERAGE_FUZZING_SCHEMA" ".properties.vulnerabilities.items.properties.links"
  verify_schema_contains_selector "$COVERAGE_FUZZING_SCHEMA" ".properties.remediations.items.properties.fixes"
  verify_schema_contains_selector "$COVERAGE_FUZZING_SCHEMA" ".properties.remediations.items.properties.summary"
  verify_schema_contains_selector "$COVERAGE_FUZZING_SCHEMA" ".properties.remediations.items.properties.diff"
}

test_should_not_contain_definitions() {
  verify_schema_doesnt_contain_selector "$COVERAGE_FUZZING_SCHEMA" ".definitions"
}

test_coverage_fuzzing_extensions() {
  local vulns=".properties.vulnerabilities.items"
  local vuln_props="$vulns.properties"

  verify_schema_contains_selector "$COVERAGE_FUZZING_SCHEMA" 'select(.properties.vulnerabilities.items.required[] | contains("location"))'

  verify_schema_contains_selector "$COVERAGE_FUZZING_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.crash_address"
  verify_schema_contains_selector "$COVERAGE_FUZZING_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.stacktrace_snippet"
  verify_schema_contains_selector "$COVERAGE_FUZZING_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.crash_state"
  verify_schema_contains_selector "$COVERAGE_FUZZING_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.crash_type"
}
